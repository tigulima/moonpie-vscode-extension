# Moon Pie, isn't it sweet? 

This extension for VS Code is made thinking on taking the stress of coding out of your system. This project is still taking it's first steps so there's only a few things Moon Pie can do, but it does it with a whole lot of love in its plastic heart.

## Features

For now, Moon Pie only displays VS Code notifications every time the user initializes the program. This notifications usually contains funny jokes and inspiring quotes. Delight the simplicity, a thing missing from the dev life!

> Tip: You can contact me to share new messages you want to see displayed by Moon Pie. I have plans on making it an automated experience but for now you are stuck with the personal touch of a human interaction. Sry.

## Requirements

Oh no, there's no requirements, realy. If you run VS Code you run Moon Pie. Happyness comes to everyone.

## Extension Settings

On future builds there will be:

* `myExtension.lenguage`: Switch lenguage from pt-br to en-us.

and not:
* `myExtension.comeAlive`: Enable it to apocalipse.

## Known Issues

The code for this extensions is DEFINETLY not pretty. I desperatly need help!

## Release Notes

Only authorized people who want to know when specific features where added can read the following information!

### 1.0.0

The birth of the glorious Moon Pie. The history now is separeted bitween before and after this event.

## The guides for oblivion

These are the documentation I used on this project.

* [Your first VS Code extension](https://code.visualstudio.com/api/get-started/your-first-extension)
* [Developer Mozila](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
* [W3 Schools](https://www.w3schools.com/js/js_htmldom_document.asp)


## Working with me :)

Feel free to fork this repo and do your magic! I don't have any criteria for how this code should be structured but when I come up with something I'll add it here.

## Come and find me!

* [Website](https://www.thiagoimai.com/)
* [LinkedIn](https://www.linkedin.com/in/tigulima/)
* [Instagram](https://www.instagram.com/tigulima.tar.gz/)
* [Twitter](https://twitter.com/Tigulima)

> ~ Free software, HELL YEAH!
